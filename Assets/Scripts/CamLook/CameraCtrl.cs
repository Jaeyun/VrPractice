﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// LayerMaskが11(UI_LOOK)を見ている場合、LookAtUICheckとの相互作用でObjectを見ているのかチェック
/// 2018.01.03 @Choi
/// </summary>
public class CameraCtrl : MonoBehaviour
{
    private Manager m_manager;
    private VrModeCtrl m_vrModeCtrl;
    private PlayerCtrl m_playerCtrl;

    [SerializeField] private GameObject objUITimeGuage;    //Cameraと一緒に動くTimeGuage Sprite
    [SerializeField] private Transform trNowLook;         //今見てるObject
    [SerializeField] private Transform trBackUpLook;      //直前まで見たObject
    [SerializeField] private Camera camNormal;
    [SerializeField] private Camera camVr;  //片方だけでも十分かも？

    private int layerMask;  //LayerMask = 11(1<<11) : UI_LOOK
    private Ray ray;        //Rayを毎回Newするのはキモいは。。。
    private RaycastHit rayHit;     //rayHitを変数に指定。

    private void Start()
    {
        m_manager = GameObject.FindWithTag(Defines.ManagerTag).GetComponent<Manager>();
        m_vrModeCtrl = m_manager.VrModeInstance;
        ray = new Ray();
        objUITimeGuage.SetActive(false);
        layerMask = 1 << 11;
    }
    private void LateUpdate()
    {
        if(m_vrModeCtrl == null)
        {
            return;
        }

        if(m_vrModeCtrl.GetIsVrModeON())
        {
            ray.origin = camVr.transform.position;         
            ray.direction = camVr.transform.forward;     
        }
        else
        {
            ray.origin = camNormal.transform.position;     
            ray.direction = camNormal.transform.forward;     
        }
        if(m_vrModeCtrl.GetIsVrModeON())
        {
            Util.DevDebug.DrawLine(ray.origin, ray.direction, Color.green, 10.0f);
        }
        else
        {
            Util.DevDebug.DrawLine(ray.origin, ray.direction, Color.green, 10.0f);
        }
        if(Physics.Raycast(ray, out rayHit, 10.0f, layerMask))
        {
            trNowLook = rayHit.transform;
            if(trNowLook.GetComponent<LookAtUICheck>() == null)
            {
                trNowLook.gameObject.AddComponent<LookAtUICheck>();
            }
            if(!trNowLook.GetComponent<LookAtUICheck>().GetSet_isCamLookAtMe)
            {
                if(trBackUpLook != trNowLook)
                {
                    if(trBackUpLook != null)
                        trBackUpLook.GetComponent<LookAtUICheck>().GetSet_isCamLookAtMe = false;
                    trBackUpLook = trNowLook;
                }
                trNowLook.GetComponent<LookAtUICheck>().GetSet_isCamLookAtMe = true;
                if(!objUITimeGuage.activeSelf)
                {
                    objUITimeGuage.SetActive(true);
                }
            }
        }
        else
        {
            if(objUITimeGuage.activeSelf)
            {
                objUITimeGuage.SetActive(false);
            }
            if(trBackUpLook != null)
            {
                trBackUpLook.GetComponent<LookAtUICheck>().GetSet_isCamLookAtMe = false;
            }
            trNowLook = trBackUpLook = null;
        }
    }

    public Transform NowLookObject
    {
        get { return trNowLook; }
    }
}
