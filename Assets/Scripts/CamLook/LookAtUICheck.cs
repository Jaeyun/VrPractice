﻿using UnityEngine;
using System.Collections;

public class LookAtUICheck : MonoBehaviour {
    [SerializeField] private bool isCamLookAtMe;
    public bool GetSet_isCamLookAtMe
    {
        get { return isCamLookAtMe; }
        set { isCamLookAtMe = value; }
    }
}
