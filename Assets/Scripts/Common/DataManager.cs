﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour {
    #region Prefabs
    [SerializeField] private UITextScript _UITextScriptPrefab;
    #endregion

    #region Instance
    private UITextScript m_UITextScript;
    #endregion


    #region Load Instance Function
    public UITextScript CreateUITextScript()
    {
        if(m_UITextScript == null)
        {
            m_UITextScript = Instantiate(_UITextScriptPrefab);
            m_UITextScript.transform.localPosition = Vector3.zero;
            m_UITextScript.transform.localScale = Vector3.one;
        }
        return m_UITextScript;
    }
    #endregion
}
