﻿public static class Defines
{
    /// <summary>
    /// Tag名がかわった場合は必ずここでも更新して下さい。
    /// 更新しないと参照している他のスクリプトで問題が発生します。
    /// 180120 @Choi
    /// </summary>
    public const string ManagerTag = "Manager";
    public const string TestCamTag = "TestCamera";


    public const char CharSplit = ',';
    public const char CharNewLine = '\n';
    public const string StrSplit = ",";
    public const string StrNewLine = "\n";

    public const string StrTrueVal = "1";
    public const string StrTrue = "true";
    public const string StrFalseVal = "0";
    public const string StrFalse = "false";

    public const int UILookLayer = 11;
}

public enum InventorySaveData
{
    ItemID = 0,
    IsEquiped,
    //今後アイテムレベルアップ情報実装する祭、このデータに追加
}
public enum EquipTableInfo
{
    id = 0,
    itemType,
    equipName,
    offenceVal,
    defenceVal,
    dexVal,
    desc,
}
public enum EquipType
{
    None = 0,
    Armor = 1,
    Weapon,
    Legs,
    Glove,
    Helm
}