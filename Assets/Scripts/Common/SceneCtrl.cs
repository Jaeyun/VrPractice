﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SceneCtrl : MonoBehaviour
{
    static string targetSceneName;
    static string beforeSceneName;

    private static SceneCtrl instance = null;
    public static SceneCtrl Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new SceneCtrl();
            }
            return instance;
        }
    }


    public void LoadNextScene(string nextSceneName)
    {
        beforeSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        UnityEngine.SceneManagement.SceneManager.LoadScene(nextSceneName);
    }

    public void BackBeforeScene()
    {
        targetSceneName = beforeSceneName;
        beforeSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
        UnityEngine.SceneManagement.SceneManager.LoadScene(targetSceneName);
        targetSceneName = string.Empty;
    }
}
