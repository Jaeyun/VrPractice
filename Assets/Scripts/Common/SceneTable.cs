﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

/// <summary>
/// SceneTable.csvに新しいキーを追加したら必ずこのEnumにもキーを追加して下さい。
/// 2018.01.21 @Choi
/// </summary>
public enum SceneName
{
    Logo,
    MainScene,
    EquipScene,
}

public class SceneTable
{
    private static string filePath = Application.dataPath + "/Resources/DataTable/SceneTable.csv";
    private static Dictionary<SceneName, string> dicSceneData = new Dictionary<SceneName, string>();

    enum SceneTableDataInfo
    {
        SceneKey = 0,
        SceneName,
        Desc,
    }

    private static SceneTable instance = null;
    public static SceneTable Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new SceneTable();
                LoadFromCSV();
            }
            return instance;
        }
    }


    public static void Init()
    {
        LoadFromCSV();
    }


    private static void LoadFromCSV()
    {
        string[] streamData = File.ReadAllLines(filePath);
        for(int i = 1; i < streamData.Length; i++)
        {
            if(string.IsNullOrEmpty(streamData[i]))
            {
                break;
            }
            string[] rowData = streamData[i].Split(Defines.CharSplit);
            SceneName tmpEnum = new SceneName();
            string tmpStr = String.Empty;
            for(int j = 0; j < rowData.Length; j++)
            {
                switch(j)
                {
                    case (int)SceneTableDataInfo.SceneKey:
                        tmpEnum = MatchKeyAndText(rowData[j]);
                        break;

                    case (int)SceneTableDataInfo.SceneName:
                        tmpStr = rowData[j];
                        break;
                }
            }
            dicSceneData.Add(tmpEnum, tmpStr);
        }
    }

    private static SceneName MatchKeyAndText(string keyVal)
    {
        string[] values = Enum.GetNames(typeof(SceneName));
        SceneName retVal = new SceneName();
        for(int i = 0; i < values.Length; i++)
        {
            if(values[i].Equals(keyVal))
            {
                retVal = (SceneName)Enum.Parse(typeof(SceneName), keyVal);
                break;
            }
        }
        return retVal;
    }


    public string GetStringData(SceneName data)
    {
        return dicSceneData[data];
    }
}
