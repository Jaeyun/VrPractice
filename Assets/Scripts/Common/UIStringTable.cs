﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;


/// <summary>
/// UIStringData.csvに新しいキーを追加したら必ずこのEnumにもキーを追加して下さい。
/// 2018.01.21 @Choi
/// </summary>
public enum UIStringData
{
    MenuBattle,//  バトル
    MenuEquip,//  装備
    MenuGacha,//  ガチャ
    MenuQuit,//  終了
    EquipHead,//  かぶと
    EquipArmor,//  よろい
    EquipWeapon,//  武器
    EquipGlove,//  グローブ
    EquipLeg,//  靴
};


public class UIStringTable
{
    private static string filePath = Application.dataPath + "/Resources/DataTable/UIStringTable.csv";
    private static Dictionary<UIStringData, string> dicUIStringData = new Dictionary<UIStringData, string>();

    enum UIStringTableInfo
    {
        StringKey = 0,
        Text,
    }

    private static UIStringTable instance = null;
    public static UIStringTable Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new UIStringTable();
            }
            return instance;
        }
    }
    public void Init()
    {
        LoadFromCSV();
    }

    private void LoadFromCSV()
    {
        string[] streamData = File.ReadAllLines(filePath);
        for(int i = 1; i < streamData.Length; i++)
        {
            if(string.IsNullOrEmpty(streamData[i]))
            {
                break;
            }
            string[] rowData = streamData[i].Split(Defines.CharSplit);
            UIStringData tmpEnum = new UIStringData();
            string tmpStr = String.Empty;
            for(int j = 0; j < rowData.Length; j++)
            {
                switch(j)
                {
                    case (int)UIStringTableInfo.StringKey:
                        tmpEnum = MatchKeyAndText(rowData[j]);
                        break;

                    case (int)UIStringTableInfo.Text:
                        tmpStr = rowData[j];
                        break;
                }
            }
            dicUIStringData.Add(tmpEnum, tmpStr);
        }
    }

    private UIStringData MatchKeyAndText(string keyVal)
    {
        string [] values = Enum.GetNames(typeof(UIStringData));
        UIStringData retVal = new UIStringData();
        for(int i = 0; i < values.Length; i++)
        {
            if(values[i].Equals(keyVal))
            {
                retVal = (UIStringData)Enum.Parse(typeof(UIStringData), keyVal);
                break;
            }
        }
        return retVal;
    }


    public string GetStringData(UIStringData data)
    {
        return dicUIStringData[data];
    }
}
