﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipCheckSet : UILookInteraction {
    [SerializeField]
    private EquipInventoryManager equipInventory;

    [SerializeField]
    private EquipType partsEquipType;

    private bool isSelected;

    private EquipItem equipedItem;

    public override void Init()
    {
        base.Init();
        GetSetIsOnceEvent = false;
    }

    public override void UILookingAction()
    {
        base.UILookingAction();
        //바라본 장비창의 타입과 일치하는지 체크
    }

    public override void UIAction()
    {
        base.UIAction();
        //最初は選択された状態に変更
        if(!isSelected)
        {
            Util.DevDebug.LogGreen(partsEquipType + "was Selected");
            isSelected = true;
        }
        else
        {
            //타입 일치시 장착.
            
            if(equipedItem == null)
            {
                //신규장착
            }
            else
            {
                //교체
            }
            isSelected = false;
        }
    }
}
