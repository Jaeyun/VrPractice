﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// このクラスは装着系アイテムテーブルのデータを読み込む
/// </summary>
public class EquipTypeItemData : MonoBehaviour {

    [SerializeField]
    private TextAsset equipItemTable;

    private Dictionary<string, EquipItem> dicEquipItemDatas;


    private static EquipTypeItemData instance = null;
    public static EquipTypeItemData Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new EquipTypeItemData();
            }
            return instance;
        }
    }

    public void Init()
    {
        dicEquipItemDatas = new Dictionary<string, EquipItem>();
        if(LoadFromData())
        {
            return;
        }
    }

    private bool LoadFromData()
    {
        string[] datas = Util.CSVParser.Instance.GetRows(equipItemTable);

        for(int i = 1; i < datas.Length; i++) //0番目はタイトルなのでSkip
        {
            if(string.IsNullOrEmpty(datas[i]))
            { 
                break;
            }
            EquipItem equipItem = new EquipItem();
            string[] rowData = datas[i].Split(',');
            for(int j = 0; j < rowData.Length; j++)
            {
                switch(j)
                {
                    case (int)EquipTableInfo.id:
                        equipItem.SetID(rowData[j]);
                        break;

                    case (int)EquipTableInfo.itemType:
                        equipItem.SetEquipType(GetEquipType(rowData[j]));
                        break;

                    case (int)EquipTableInfo.equipName:
                        equipItem.SetName(rowData[j]);
                        break;

                    case (int)EquipTableInfo.offenceVal:
                        equipItem.SetOffenceVal(int.Parse(rowData[j]));
                        break;

                    case (int)EquipTableInfo.defenceVal:
                        equipItem.SetDefenceVal(int.Parse(rowData[j]));
                        break;

                    case (int)EquipTableInfo.dexVal:
                        equipItem.SetDexVal(int.Parse(rowData[j]));
                        break;

                    case (int)EquipTableInfo.desc:
                        equipItem.SetDesc(rowData[j]);
                        break;
                }
            }
            dicEquipItemDatas.Add(equipItem.GetID(), equipItem);
        }

        return true;
    }

    private EquipType GetEquipType(string chkVal)
    {
        int chkCode = int.Parse(chkVal);
        EquipType type = EquipType.None;
        switch(chkCode)
        {
            case (int)EquipType.None:
                type = EquipType.None;
                break;

            case (int)EquipType.Armor:
                type = EquipType.Armor;
                break;

            case (int)EquipType.Weapon:
                type = EquipType.Weapon;
                break;

            case (int)EquipType.Legs:
                type = EquipType.Legs;
                break;

            case (int)EquipType.Glove:
                type = EquipType.Glove;
                break;

            case (int)EquipType.Helm:
                type = EquipType.Helm;
                break;
        }
        return type;
    }
}
