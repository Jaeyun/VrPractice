﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipItemPrefabSet : MonoBehaviour {
    public GameObject[] equipItemPrefabs;

    private Dictionary<string, EquipItem> equipDatas = new Dictionary<string, EquipItem>();

    public void Init()
    {
        //StartCoroutine(InitPrefabProc());
        if(InitPrefabs())
        {
            return;
        }
    }

    private bool InitPrefabs()
    {
        for(int i = 0; i < equipItemPrefabs.Length; i++)
        {
            equipDatas.Add(equipItemPrefabs[i].name, equipItemPrefabs[i].GetComponent<EquipItem>());
            SetEquipmentData(equipItemPrefabs[i].name, equipItemPrefabs[i].GetComponent<EquipItem>());
        }
        return true;
    }

    public void SetEquipmentData(string keyVal, EquipItem equipment)
    {
        if(equipDatas.ContainsKey(keyVal))
        {
            equipDatas[keyVal] = equipment;
        }
    }
}
