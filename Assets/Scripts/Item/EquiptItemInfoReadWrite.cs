﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

/// <summary>
/// 装備系アイテムRead/Write
/// こちらは今後サーバー通信によってDBから読み込むように修正するべき。
/// (かなり時間はかかるかもしれないけど。。。)
/// 2018.01.14 @Choi
/// </summary>
public class EquiptItemInfoReadWrite : InfoReadWrite
{
    //装備アイテムに入るべきの情報
    //ID
    //装着？
    //入手時間
    //その他、実装する次第レベル、EXPなどなど。。。
    private const string _titleItemID = "ItemID";
    private const string _titleIsEquiped = "IsEquiped";

    public override void InitData()
    {
        fileName = "InventoryInfo";
        nowData = _titleItemID + strSplit + _titleIsEquiped + strNewLine;
        base.InitData();
    }
}