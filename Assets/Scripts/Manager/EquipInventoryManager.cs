﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipInventoryManager : MonoBehaviour
{
    [SerializeField]
    private EquipTypeItemData equiptypeItemsData;  // DataLoaderr -> 最優先に読み込む
    [SerializeField]
    private EquipItemPrefabSet equipItemPrefabSet;  // Prefab Loader -> 最優先に読み込む
    [SerializeField]
    private UserInventoryDataManager mgrUserInventory;

    private static EquipInventoryManager instance = null;
    public static EquipInventoryManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new EquipInventoryManager();
            }
            return instance;
        }
    }


    public void Init()
    {
        //0. Load Item Datas
        equiptypeItemsData.Init();

        //1. Load Prefab Datas
        equipItemPrefabSet.Init();

        //2. Load Save Inventory Data
        mgrUserInventory.Init();
    }
}
