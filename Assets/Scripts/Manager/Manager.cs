﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
    [Header("共用オブジェクトContrl")]
    [SerializeField]
    private VrModeCtrl m_vrModeCtrl;
    [SerializeField]
    private PlayerCtrl m_playerCtrl;
    [SerializeField]
    private FadeCubeCtrl m_fadeCubeCtrl;
    [SerializeField]
    private DataManager m_dataManager;

    private CameraCtrl m_CameraCtrl;



    [Header("基本データContrl")]
    [SerializeField]
    private EquipInventoryManager mgrEquipInventory;


    [Header("内部メンバー")]
    private bool isQuitApp;
    public bool IsQuitApp
    {
        get { return isQuitApp; }
        set { isQuitApp = value; }
    }

    // Use this for initialization
    private void Awake ()
    {
        DontDestroyOnLoad(m_playerCtrl);
        DontDestroyOnLoad(m_vrModeCtrl);
        DontDestroyOnLoad(m_fadeCubeCtrl);
        DontDestroyOnLoad(m_dataManager);
        DontDestroyOnLoad(this);

        Init();
    }

    private void Init()
    {
        //システムで使うテキストロード
        UIStringTable.Instance.Init();
        m_CameraCtrl = FindObjectOfType<CameraCtrl>();
        m_vrModeCtrl.Init();
        m_fadeCubeCtrl.Init();
        
        //1. ここで必要な初期データをセッティングする。
        mgrEquipInventory.Init();

        //1.1 この作業がおわったらVRモード転換を案内してもいいかと

        //2.　ここでユーザー情報を読み込む
        // UserInfo(まだなし)
        // CharacterInfo
        // Inventory(まだなし)
        // StageInfo(まだなし)

        //2.1　上記の作業によって、今後ローディングに時間がかかることになると思われるので、
        //    できるのであればローディングProgress Barを入れるのが良いかと。

        //3. 2までの作業が終わってからはいつでもIn Game入場しても構わない。

    }

    public VrModeCtrl VrModeInstance
    {
        get { return m_vrModeCtrl; }
    }

    public FadeCubeCtrl FadeCubeCtrlInstance
    {
        get { return m_fadeCubeCtrl; }
    }

    public PlayerCtrl PlayerCtrlInstance
    {
        get { return m_playerCtrl; }
    }

    public DataManager DataManagerInstance
    {
        get { return m_dataManager; }
    }

    public CameraCtrl CameraCtrlInstance
    {
        get { return m_CameraCtrl; }
    }

    public void QuitApp()
    {
        isQuitApp = true;
        m_fadeCubeCtrl.DoFadeInAction(QuitAppAction);
    }

    private void QuitAppAction()
    {
        Application.Quit();
    }
    private void OnDestroy()
    {
        if(m_dataManager != null)
        {
            Destroy(m_dataManager.gameObject);
            m_dataManager = null;
        }
        if(m_vrModeCtrl != null)
        {
            Destroy(m_vrModeCtrl.gameObject);
            m_vrModeCtrl = null;
        }
        if(m_fadeCubeCtrl != null)
        {
            Destroy(m_fadeCubeCtrl.gameObject);
            m_fadeCubeCtrl = null;
        }
        if(m_playerCtrl != null)
        {
            Destroy(m_playerCtrl.gameObject);
            m_playerCtrl = null;
        }
    }
}
