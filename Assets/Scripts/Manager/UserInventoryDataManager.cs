﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class UserInventoryDataManager : MonoBehaviour {
    private const string folderName = "\\SaveData\\";
    private const string fileName = "InventoryInfo";
    private const string fileType = ".txt"; //出来るのであれば別の拡張子にして保安性を少しでもあげたい。。。
    private string strDevicePath;
    private string strFullPath;

    private List<EquipItem> userInventoryList;  //ユーザー所持アイテム全体情報

    private static UserInventoryDataManager instance = null;
    public static UserInventoryDataManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new UserInventoryDataManager();
            }
            return instance;
        }
    }

    public void Init()
    {
        #region Load File
        strDevicePath = Application.dataPath;
        userInventoryList = new List<EquipItem>();

        //1. Check Save Folder Exist
        //(if Not Exist, Create Directory)
        //Folder Path = Device 
        strFullPath = strDevicePath + folderName;
        DirectoryInfo dirInfo = new DirectoryInfo(strFullPath);
        if(dirInfo == null)
        {
            dirInfo.Create();
        }
        
        //2. Check Save Data Exist
        //(if Not Exit, Create File And Write Default Data)
        strFullPath += fileName + fileType;
        FileInfo fileInfo = new FileInfo(strFullPath);
        if(fileInfo == null)
        {
            fileInfo.Create();
        } 
        #endregion
        if(LoadFromSaveData())
        {
            return;
        }
    }

    private bool LoadFromSaveData()
    {
        string[] saveDatas = File.ReadAllLines(strFullPath);

        if(saveDatas.Length > 1)//0番目はタイトルなので、パス
        {
            for(int i = 1; i < saveDatas.Length; i++)
            {
                string[] rowData = saveDatas[i].Split(',');
                EquipItem tmpEquipItem = new EquipItem();
                for(int j = 0; j < rowData[j].Length; j++)
                {
                    switch(j)
                    {
                        case (int)InventorySaveData.ItemID:
                            tmpEquipItem.SetID(rowData[j]);
                            break;

                        case (int)InventorySaveData.IsEquiped:
                            tmpEquipItem.GetSetIsEquipedNow = (rowData[j].Equals(Defines.StrTrue) || rowData[j].Equals(Defines.StrTrueVal)) 
                                ? true : false;
                            break;
                    }
                }

            }
        }
        return true;
    } 
}
