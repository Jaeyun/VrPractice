﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// LogoSceneのシーンチェンジを定義
/// 2018.01.02 @Choi
/// </summary>
public class LogoScene : Scene
{
    [SerializeField]
    private UILookSceneChangeButton sceneChangeButton;

    public override void Init()
    {
        nextSceneName = "1.MainScene";
        base.Init();
        m_vrModeCtrl.CreateViewModeTextureContainer();
        m_fadeCubeCtrl.ActiveFadeCube(true);
        StartCoroutine(CheckIsVrModeProc());
    }
    IEnumerator CheckIsVrModeProc()
    {
        while(!m_vrModeCtrl.GetIsVrModeON())
        {
            yield return new WaitForFixedUpdate();
        }
        sceneChangeButton.SetData(nextSceneName);
        m_fadeCubeCtrl.DoFadeInAction(ActivateSceneChangeButton);
    }
    public void ActivateSceneChangeButton()
    {
        sceneChangeButton.gameObject.SetActive(true);
    }
}
