﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 相続するChildeSceneではシーンを変更するObjectと、シーン名だけ指定すれば良い
/// </summary>
public class Scene : MonoBehaviour {
    protected Manager m_manager;
    protected FadeCubeCtrl m_fadeCubeCtrl;
    protected VrModeCtrl m_vrModeCtrl;

    protected string nextSceneName; //Tableでなんとか出来ないのかな...

    private void Awake()
    {
    }

    private void Start()
    {
        Init();
    }


    // VrCotntrollerとFadeObjectのOn/OFFは必要するシーンと必要ではないシーンがあるのでここでは初期化だけする。
    public virtual void Init()
    {
        CheckExistTestCam();
        m_manager = GameObject.FindWithTag(Defines.ManagerTag).GetComponent<Manager>();
        if(m_vrModeCtrl == null)
        {
            m_vrModeCtrl = m_manager.VrModeInstance;
        }
        if(m_fadeCubeCtrl == null)
        {
            m_fadeCubeCtrl = m_manager.FadeCubeCtrlInstance;
        }
    }


    private void CheckExistTestCam()
    {
        GameObject testCamera = GameObject.FindWithTag(Defines.TestCamTag);
        if(testCamera != null)
        {
            testCamera.SetActive(false);
        }
    }
}
