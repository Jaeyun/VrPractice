﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Util
{
    /// <summary>
    /// CSVファイルから読み込んで行を返す。
    /// 今後コラムも返せるように改善しないと行けない。
    /// 18.01.03 @Choi
    /// </summary>
    public class CSVParser
    {
        private static CSVParser instance = null;
        public static CSVParser Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new CSVParser();
                }
                return instance;
            }
        }

        public string[] GetRows(TextAsset textAsset)
        {
            string text = textAsset.text;
            return text.Split('\n');
        }
    }
}
