﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CustomMenuItem
{
#if UNITY_EDITOR
    public class CustomMenuSceneStarter
    {
        [UnityEditor.MenuItem("SceneStart/StartScene &s")]
        // Use this for initialization
        static void StartScene()
        {
            UnityEditor.EditorApplication.OpenScene("Assets/Scenes/0.LogoScene.unity");
            UnityEditor.EditorApplication.isPlaying = true;
        }


        [UnityEditor.MenuItem("VrModeCtrl/ChangeToVRMode &v")]
        // Use this for initialization
        static void ChangeToVRMode()
        {
            VrModeCtrl _vrModeCtrl = GameObject.FindObjectOfType<VrModeCtrl>();
            if(_vrModeCtrl == null)
            {
                return;
            }
            if(_vrModeCtrl.DeviceOrientationForDev != DeviceOrientation.LandscapeLeft)
            {
                _vrModeCtrl.DeviceOrientationForDev = DeviceOrientation.LandscapeLeft;
            }
            else
            {
                _vrModeCtrl.DeviceOrientationForDev = DeviceOrientation.Unknown;
            }
        }
    }
#endif
}
