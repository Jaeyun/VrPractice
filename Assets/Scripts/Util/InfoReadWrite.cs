﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Util
{
    /// <summary>
    /// 注意::相続クラスはSetFileNmaeでファイル名を指定すること！
    /// *.txtファイルのRead/Writeを担当。
    /// 18.01.14 @choi
    /// </summary>
    public class InfoReadWrite : MonoBehaviour
    {
        private string strPath = "";
        private const string folderName = "\\SaveData\\";
        private const string fileType = ".txt";
        private const char charNewLine = '\n';
        private const char charSplit = ',';

        public const string strNewLine = "\n";
        public const string strSplit = ",";

        private string strFullPath = "";

        protected string fileName = "";
        protected string nowData = "";


        public string GetPath()
        {
            return strPath;
        }

        private void Awake()
        {
            strPath = Application.dataPath + folderName;
        }
        private void Start()
        {
            InitData();
        }
        public void SetFileAndPath(string _fileName)
        {
            fileName = _fileName;
            strFullPath = strPath + fileName + fileType;
            DirectoryInfo dirInfo = new DirectoryInfo(strPath);
            if(!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        /// <summary>
        /// 各々相続クラスで再定義。
        /// fileNameを指定すること。
        /// </summary>
        public virtual void InitData()
        {
            SetFileAndPath(fileName);
            if(!File.Exists(strFullPath)) // 지정된 경로에 파일이 존재하는지 검사
            { 
                FileCreate();
            }
        }

        /// <summary>
        /// 各々相続クラスで再定義。セーブするデータを作成して、最後にBaseとして更に呼ぶこと。
        /// </summary>
        public virtual void WriteFile()
        {
            WriteFileProc(nowData);
        }
        private void WriteFileProc(string strData)
        {
            FileStream fileStream = new FileStream(strFullPath, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(fileStream, Encoding.UTF8);
            writer.WriteLine(strData);
            writer.Close();
            fileStream.Close();

            DevDebug.Log(strFullPath + " was Writed");
        }

        public string [] ReadFile()
        {
            DevDebug.Log(strFullPath + " In ReadFile");
            string [] retVal;
            string tmpVal = "";
            FileStream fileStream = new FileStream(strFullPath, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(fileStream, Encoding.UTF8);
            reader.BaseStream.Seek(0, SeekOrigin.Begin);
            while(reader.Peek() > -1)
            {
                tmpVal += reader.ReadLine() + "\n";
            }
            reader.Close();
            fileStream.Close();

            DevDebug.Log(tmpVal);
            retVal = tmpVal.Split(charNewLine);
            return retVal;
        }

        private void FileCreate()
        {
            FileInfo file = new FileInfo(strFullPath);

            FileStream fileStream = file.Create();
            fileStream.Close();

            WriteFileProc(nowData);
        }
    }
}
