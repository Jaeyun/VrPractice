﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// NGUIでVRモードを対応するためのスクリプト
/// UI Rootレベルに追加すること。
/// 注意：必ずCameraComponentがUI Root直下に2つあること。
/// 2018.01.03 @Choi
/// </summary>
enum CameraStatus
{
    Left = 0,
    Right = 1,
}
public class UIVrMode : MonoBehaviour {
    public Camera[] cameras;
    private Manager m_manager;
    private VrModeCtrl m_vrModeCtrl;
    Rect rectVrCamLeft = new Rect(0f, 0f, 0.5f, 1f);
    Rect rectVrCamRight = new Rect(0.5f, 0f, 0.5f, 1f);
    Rect rectNormalCam = new Rect(0f, 0f, 1f, 1f);
    // Use this for initialization
    void Start ()
    {
        m_manager = GameObject.FindWithTag(Defines.ManagerTag).GetComponent<Manager>();
        if(m_vrModeCtrl == null)
        {
            m_vrModeCtrl = m_manager.VrModeInstance;
        }
        cameras = GetComponentsInChildren<Camera>();
        SetNormalUIModeCamera();
    }
	
	// Update is called once per frame
	void LateUpdate ()
    {
        if(m_vrModeCtrl == null)
        {
            return;
        }
        if(m_vrModeCtrl.GetIsUIVrModeON())
        {
            SetVrUIModeCamera();
        }
        if(!m_vrModeCtrl.GetIsUIVrModeON())
        {
            SetNormalUIModeCamera();
        }
    }
    

    void SetVrUIModeCamera()
    {
        cameras[(int)CameraStatus.Right].enabled = true;
        cameras[(int)CameraStatus.Left].rect = rectVrCamLeft;
        cameras[(int)CameraStatus.Right].rect = rectVrCamRight;
    }
    void SetNormalUIModeCamera()
    {
        cameras[(int)CameraStatus.Right].enabled = false;
        cameras[(int)CameraStatus.Left].rect = rectNormalCam;
    }
}
