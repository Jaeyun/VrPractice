﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrModeCtrl : MonoBehaviour
{
    public ViewModeTextureContainer viewModeTexturePrefab;
    [SerializeField]
    private GameObject VrModeCamObj;
    [SerializeField]
    private GameObject NormalCamObj;
    [SerializeField]
    private ViewModeTextureContainer m_viewModeTextureContainer;
    [SerializeField]
    private DeviceOrientation deviceOrientation;
    [SerializeField]
    private Manager m_Manager;


    private bool isVrModeOn;
    private bool isUIVrModeOn;
    private bool isChangeVrMode;
    private bool isNowProc;
    private bool isUseCountDown;
    private Action ChangeFinishAction;
    private UIRoot uiRoot;


    public void Init()
    {
        m_Manager = GetComponentInParent<Manager>();
        CreateViewModeTextureContainer();
        ChangeViewMode(true, true);
    }

    public void CreateViewModeTextureContainer()
    {
        if(m_viewModeTextureContainer == null)
        {
            uiRoot = FindObjectOfType<UIRoot>();
            m_viewModeTextureContainer = Instantiate(viewModeTexturePrefab) as ViewModeTextureContainer;
            m_viewModeTextureContainer.transform.parent = uiRoot.transform;
            m_viewModeTextureContainer.transform.localScale = Vector3.one;
            m_viewModeTextureContainer.SetActiveTexture(false);
        }
        m_viewModeTextureContainer.SetActiveTexture(m_viewModeTextureContainer.gameObject.activeSelf);
    }
    public bool GetIsVrModeON()
    {
        return isVrModeOn;
    }
    public bool GetIsUIVrModeON()
    {
        return isUIVrModeOn;
    }


    public void ChangeViewMode(bool isChangeVr, bool _isUseCountDown, Action changeFinishAction = null)
    {
        isChangeVrMode = isChangeVr;
        isNowProc = true;
        isUseCountDown = _isUseCountDown;
        ChangeFinishAction = changeFinishAction;
        StartCoroutine(ChangeViewModeProc());
    }

    private void ChangeView()
    {
        NormalCamObj.SetActive(!isChangeVrMode);
        VrModeCamObj.SetActive(isChangeVrMode);

        isUIVrModeOn = isChangeVrMode;
        if(isUseCountDown)
        {
            StartCoroutine(CountProc());
            return;
        }
        m_viewModeTextureContainer.SetActiveTexture(false);
        if(ChangeFinishAction != null)
        {
            ChangeFinishAction();
        }
    }

    IEnumerator ChangeViewModeProc()
    {
        float fCountTime = 0f;
        int maxLength = m_viewModeTextureContainer.GetChangeGuideImagesLength()- 1;
        int index = isChangeVrMode ? 0 : maxLength;
        if(m_Manager.IsQuitApp)
        {
            yield break;
        }
        if(m_viewModeTextureContainer != null
            && !m_viewModeTextureContainer.IsActiveTexture())
        {
            m_viewModeTextureContainer.SetActiveTexture(true);
        }
        while(isNowProc)
        {
            m_viewModeTextureContainer.SetchangeGuideImageTexture(index);
            yield return new WaitForFixedUpdate();
            fCountTime += Time.deltaTime;
            if(fCountTime > 0.5f)
            {
                if(isChangeVrMode && index >= maxLength)
                {
                    index = 0;
                }
                else if(!isChangeVrMode && index <= 0)
                {
                    index = maxLength;
                }
                else if(isChangeVrMode && index < maxLength)
                {
                    index++;
                }
                else if(!isChangeVrMode && index > 0)
                {
                    index--;
                }
                fCountTime = 0f;
            }
#if UNITY_EDITOR
            if((isChangeVrMode && deviceOrientation == DeviceOrientation.LandscapeLeft)
                || (!isChangeVrMode && deviceOrientation == DeviceOrientation.Portrait))
            {
                isNowProc = false;
            }
#else
            if((isChangeVrMode && Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
                || (!isChangeVrMode && Input.deviceOrientation == DeviceOrientation.Portrait))
            {
                isNowProc = false;
            }
#endif
        }
        ChangeView();
    }
    IEnumerator CountProc()
    {
        if(m_Manager.IsQuitApp)
        {
            yield break;
        }
        if(m_viewModeTextureContainer != null 
            &&!m_viewModeTextureContainer.IsActiveTexture())
        {
            m_viewModeTextureContainer.SetActiveTexture(true);
        }
        int nCountTime = 0;
        int maxLength = m_viewModeTextureContainer.GetCountDownImagesLength() - 1;
        while(nCountTime <= maxLength)
        {
            m_viewModeTextureContainer.SetCountDownImageTexture(nCountTime);
            nCountTime++;
#if UNITY_EDITOR
            yield return new WaitForSeconds(0.2f);
#else
            yield return new WaitForSeconds(1f);
#endif
        }
        m_viewModeTextureContainer.SetActiveTexture(false);
        isVrModeOn = isChangeVrMode;
        if(ChangeFinishAction != null)
        {
            ChangeFinishAction();
        }
        Util.DevDebug.LogGreen("isVrModeOn :: " + isVrModeOn + ", isChangeVrMode :: " + isChangeVrMode);
    }

    /// <summary>
    /// User Only Develop Mode, For VrMode Change
    /// 180121 @Choi
    /// </summary>
    public DeviceOrientation DeviceOrientationForDev
    {
        get { return deviceOrientation; }
        set { deviceOrientation = value; }
    }
}
