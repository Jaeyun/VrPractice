﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookInventory : UILookUseText
{
    public override void Init()
    {
        base.Init();
        fLookMaxTime = 3f;
        GetSetIsOnceEvent = false;
    }
    public override void SetThisUIText()
    {
        base.SetThisUIText();
        m_UITextScript.SetData(
            UIStringTable.Instance.GetStringData(UIStringData.MenuEquip),
            true);
    }
}
