﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookQuitApp : UILookUseText
{
    private VrModeCtrl m_vrModeCtrl;
    private FadeCubeCtrl m_fadeCubeCtrl;


    public override void Init()
    {
        if(m_vrModeCtrl == null)
        {
            m_vrModeCtrl = m_manager.VrModeInstance;
        }
        if(m_fadeCubeCtrl == null)
        {
            m_fadeCubeCtrl = m_manager.FadeCubeCtrlInstance;
        }

        base.Init();
        fLookMaxTime = 3f;
        GetSetIsOnceEvent = true;
    }

    public override void UIAction()
    {
        base.UIAction();
        m_fadeCubeCtrl.DoFadeInAction(ChangeViewAction);
    }

    private void ChangeViewAction()
    {
        m_vrModeCtrl.ChangeViewMode(false, true, ()=> {
            m_manager.QuitApp();
        });
    }

    public override void SetThisUIText()
    {
        base.SetThisUIText();
        m_UITextScript.SetData(
            UIStringTable.Instance.GetStringData(UIStringData.MenuQuit), 
            true);
    }
}
