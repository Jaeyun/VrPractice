﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UILookSceneChangeButton : UILookInteraction
{
    private string targetSceneName;
    private FadeCubeCtrl m_fadeCubeCtrl;
    
    public void SetData(string SceneName)
    {
        m_fadeCubeCtrl = m_manager.FadeCubeCtrlInstance;
        targetSceneName = SceneName;
        Init();
        GetSetIsOnceEvent = true;
    }

    public override void UIAction()
    {
        base.UIAction();
        m_fadeCubeCtrl.DoFadeOutInAction(NextSceneLoadction, null);
    }

    private void NextSceneLoadction()
    {
        SceneCtrl.Instance.LoadNextScene(SceneTable.Instance.GetStringData(SceneName.MainScene));
    }
}
