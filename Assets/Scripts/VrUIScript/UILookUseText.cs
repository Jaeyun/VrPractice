﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookUseText : UILookInteraction
{
    protected UITextScript m_UITextScript;
    protected CameraCtrl m_CameraCtrl;

    public override void Init()
    {
        m_CameraCtrl = m_manager.CameraCtrlInstance;
        base.Init();
        m_UITextScript = m_manager.DataManagerInstance.CreateUITextScript();
        m_UITextScript.gameObject.SetActive(false);
    }

    public override void UILookingAction()
    {
        base.UILookingAction();
        if(!m_UITextScript.gameObject.activeSelf
            &&m_CameraCtrl.NowLookObject == transform)
        {
            SetThisUIText();
            m_UITextScript.gameObject.SetActive(true);
        }
        return;
    }

    public override void UINoneLookingAction()
    {
        base.UINoneLookingAction();
        if(m_UITextScript.gameObject.activeSelf 
            && m_CameraCtrl.NowLookObject == null)
        {
            m_UITextScript.gameObject.SetActive(false);
        }
        return;
    }
    /// <summary>
    /// m_UITextScript.SetData(UIStringData, isCenter?);
    /// </summary>
    public virtual void SetThisUIText()
    {
    }
}
