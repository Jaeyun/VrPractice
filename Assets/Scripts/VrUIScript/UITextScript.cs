﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITextScript : MonoBehaviour {
    [SerializeField]
    private UILabel m_UiText;

    int m_count;

    public void SetData(string val, bool isAlignmentCenter = true)
    {
        if(!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
        m_UiText.text = val;
        m_UiText.alignment = isAlignmentCenter ? NGUIText.Alignment.Center : NGUIText.Alignment.Left;
    }
}
