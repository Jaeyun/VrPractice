﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewModeTextureContainer : MonoBehaviour
{
    [SerializeField]
    private UITexture baseUITexture;
    [SerializeField]
    private Texture[] changeGuideImages;
    [SerializeField]
    private Texture[] CountDownImages;

    public int GetChangeGuideImagesLength()
    {
        return changeGuideImages.Length;
    }
    public int GetCountDownImagesLength()
    {
        return CountDownImages.Length;
    }

    public void SetchangeGuideImageTexture(int index)
    {
        baseUITexture.mainTexture = changeGuideImages[index];
    }

    public void SetCountDownImageTexture(int index)
    {
        baseUITexture.mainTexture = CountDownImages[index];
    }


    public void SetActiveTexture(bool isOn)
    {
        baseUITexture.gameObject.SetActive(isOn);
    }
    public bool IsActiveTexture()
    {
        return baseUITexture.gameObject.activeSelf;
    }
}
